import {useEffect, useRef} from 'react';

const usePrevious = (val: string | number) => {
  const prevVal = useRef(val);
  useEffect(() => {
    prevVal.current = val;
  }, [val])
  return prevVal.current;
}

export default usePrevious;
