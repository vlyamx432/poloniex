import {useEffect, useMemo, FC} from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

import styles from './Modal.module.css';

interface IProps {
  open: boolean,
  onClose(): void,
}

const Modal: FC<IProps> = ({children, open, onClose}) => {
  const el = useMemo(() => document.createElement("div"), []);

  useEffect(() => {
    el.classList.add(styles.modalWrap);
    document.body.appendChild(el);
    return () => {
      document.body.removeChild(el);
    };
  }, [el]);

  return ReactDOM.createPortal(
    <>
      <div onClick={onClose}
           className={classNames({
             [styles.backdrop]: true,
             [styles.backdropVisible]: open
           })}/>
      <div className={classNames({
        [styles.modal]: true,
        [styles.modalVisible]: open
      })}>{children}</div>
    </>
    , el);
}

export default Modal;
