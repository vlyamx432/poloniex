import React from 'react';

import {Ticker} from '@types';
import Loader from '@components/Loader/Loader';
import Row from './Row';
import styles from './Table.module.css';

interface IProps {
  data: Array<Ticker>,
  loading: boolean,
  activeRowData: Ticker | null,

  onRowClick(data: Ticker): void,
}

const Table: React.FC<IProps> = ({data, activeRowData, onRowClick, loading}: IProps) => {
  return (
    <div className={styles.tableWrap}>
      <table className={styles.table}>
        <thead>
        <tr>
          <th>name</th>
          <th>last</th>
          <th>highestBid</th>
          <th>percentChange</th>
        </tr>
        </thead>
        <tbody>
        {loading && <tr className={styles.loading}>
          <td colSpan={4}>
            <Loader/>
          </td>
        </tr>}
        {!loading && data.map((ticker) => {
          return (<Row key={ticker.id}
                       onClick={onRowClick}
                       data={ticker}
                       active={ticker.id === activeRowData?.id}/>
          )
        })}

        </tbody>
      </table>
    </div>
  )
}

export default Table;
