import React, {FC} from 'react';

import Modal from "@components/Modal/Modal";
import {Ticker} from "@types";
import styles from './ModalInfo.module.css';

interface IProps {
  open: boolean,
  data: Ticker | null,

  onClose(): void,
}

const ModalInfo: FC<IProps> = ({open, onClose, data}) => {
  return (
    <Modal open={open} onClose={onClose}>
      <ul className={styles.tickerData}>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Name:</strong>
          <span className={styles.tickerDataText}>{data?.name}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Highest Bid:</strong>
          <span className={styles.tickerDataText}>{data?.highestBid}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>High 24hr:</strong>
          <span className={styles.tickerDataText}>{data?.high24hr}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Last:</strong>
          <span className={styles.tickerDataText}>{data?.last}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Base Volume:</strong>
          <span className={styles.tickerDataText}>{data?.baseVolume}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Is Frozen:</strong>
          <span className={styles.tickerDataText}>{data?.isFrozen}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Low 24hr:</strong>
          <span className={styles.tickerDataText}>{data?.low24hr}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Lowest Ask:</strong>
          <span className={styles.tickerDataText}>{data?.lowestAsk}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Percent Change:</strong>
          <span className={styles.tickerDataText}>{data?.percentChange}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Post Only:</strong>
          <span className={styles.tickerDataText}>{data?.postOnly}</span>
        </li>
        <li className={styles.tickerDataItem}>
          <strong className={styles.tickerDataText}>Quote Volume:</strong>
          <span className={styles.tickerDataText}>{data?.quoteVolume}</span>
        </li>
      </ul>
    </Modal>
  )
}

export default ModalInfo;
